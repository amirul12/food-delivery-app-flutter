import 'package:flutter_food_delivery_app/model/place_autocomplete_model.dart';
import 'package:flutter_food_delivery_app/model/place_model.dart';

abstract class BasePlaceRepository {
  Future<List<PlaceAutocomplete>?> getAutocomplete(String searchInput) async {}

  Future<Place?> getPlace(String placeId) async {}
}
