// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:flutter/material.dart';
import 'package:flutter_food_delivery_app/model/place_autocomplete_model.dart';
import 'package:flutter_food_delivery_app/model/place_model.dart';
import 'package:flutter_food_delivery_app/repository/place/base_place_repository.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

//https://developers.google.com/maps/documentation/places/web-service/autocomplete

class PlaceRepository extends BasePlaceRepository {
  final String key = "AIzaSyD6JyiKDjH5gek20G9u4S5VEE_igX_Ii5A";
  final String type = "geocode";

  @override
  Future<List<PlaceAutocomplete>> getAutocomplete(String searchInput) async {
    final String url =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$searchInput&types=$type&key=$key';

    var response = await http.get(Uri.parse(url), headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    });

    // String data =
    //     await DefaultAssetBundle.of(context).loadString("json/parse.json");
    // var json = convert.jsonDecode(response.body);
    // print(json);
    // var results = json['predictions'] as List;

    // return results.map((place) => PlaceAutocomplete.fromJson(place)).toList();

    final list = List.generate(
        20,
        (index) => PlaceAutocomplete(
            description: "Dhaka Kwaran $index", placeId: "$index"));

    return list;
  }

  @override
  Future<Place> getPlace(String placeId) async {
    final String url =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$key';
    var response = await http.get(Uri.parse(url), headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    });

    var json = convert.jsonDecode(response.body);
    print(json);
    var results = json['result'] as Map<String, dynamic>;

    return Place.fromJson(results);
  }
}
