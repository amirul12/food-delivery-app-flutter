import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_food_delivery_app/model/restaurant_model.dart';
import 'package:flutter_food_delivery_app/screens/basket/basket_screen.dart';
import 'package:flutter_food_delivery_app/screens/delivery_time/delivery_time_screen.dart';
import 'package:flutter_food_delivery_app/screens/edit_basket/edit_basket_screen.dart';
import 'package:flutter_food_delivery_app/screens/filter/filter_screen.dart';
import 'package:flutter_food_delivery_app/screens/home/home_screen.dart';
import 'package:flutter_food_delivery_app/screens/location/location_screen.dart';
import 'package:flutter_food_delivery_app/screens/restaurant_details/restaurant_details_screen.dart';
import 'package:flutter_food_delivery_app/screens/restaurant_list/restaurant_listing_screen.dart';
import 'package:flutter_food_delivery_app/screens/voucher/voucher_screen.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    print('The Router is: ${settings.name}');

    switch (settings.name) {
      case '/':
        return HomeScreen.route();
      case LocationScreen.routeName:
        return LocationScreen.route();
      case BasketScreen.routeName:
        return BasketScreen.route();
      case EditBasketScreen.routeName:
        return EditBasketScreen.route();
      case DeliveryTimeScreen.routeName:
        return DeliveryTimeScreen.route();
      case FilterScreen.routeName:
        return FilterScreen.route();
      case RestaurantDetailsScreen.routeName:
        return RestaurantDetailsScreen.route(
            restaurant: settings.arguments as Restaurant);

      case RestaurantListingScreen.routeName:
        return RestaurantListingScreen.route(
            restaurants: settings.arguments as List<Restaurant>);
      case VoucherScreen.routeName:
        return VoucherScreen.route();

      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text("error"),
              ),
            ),
        settings: const RouteSettings(name: '/error'));
  }
}
