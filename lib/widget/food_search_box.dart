import 'package:flutter/material.dart';
import 'package:flutter_food_delivery_app/screens/filter/filter_screen.dart';

class FoodSearchBox extends StatelessWidget {
  const FoodSearchBox({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              textAlign: TextAlign.start,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                hintText: "What would you like to eat?",
                suffixIcon: Icon(Icons.search),
                contentPadding: const EdgeInsets.only(
                  left: 20,
                  bottom: 5,
                  top: 12.5,
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.white),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5), color: Colors.white),
            width: 50,
            height: 50,
            child: IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, FilterScreen.routeName);
                },
                icon: Icon(
                  Icons.menu,
                  color: Theme.of(context).primaryColor,
                )),
          )
        ],
      ),
    );
  }
}
