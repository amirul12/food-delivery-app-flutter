import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/filters/filters_bloc.dart';
import 'package:flutter_food_delivery_app/model/models.dart';

class CustomCategoryFilter extends StatelessWidget {
  const CustomCategoryFilter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FiltersBloc, FiltersState>(
      builder: (context, state) {
        if (state is FiltersLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is FiltersLoaded) {
          return ListView.builder(
              shrinkWrap: true,
              itemCount: state.filter.categoryFilters.length,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  width: double.infinity,
                  margin: const EdgeInsets.only(top: 10),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        state.filter.categoryFilters[index].category.name,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      SizedBox(
                        child: Checkbox(
                            value: state.filter.categoryFilters[index].value,
                            onChanged: (bool? value) {
                              context.read<FiltersBloc>().add(
                                    CategoryFilterUpdated(
                                      categoryFilter: state
                                          .filter.categoryFilters[index]
                                          .copyWith(
                                              value: !state
                                                  .filter
                                                  .categoryFilters[index]
                                                  .value),
                                    ),
                                  );
                            }),
                      ),
                    ],
                  ),
                );
              });
        } else {
          return Text("Something went wrong");
        }
      },
    );
  }
}
