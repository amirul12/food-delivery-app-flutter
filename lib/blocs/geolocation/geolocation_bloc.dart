import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/repository/geolocation/geolocation_repository.dart';
import 'package:geolocator/geolocator.dart';
part 'geolocation_event.dart';
part 'geolocation_state.dart';

class GeolocationBloc extends Bloc<GeolocationEvent, GeolocationState> {
  GeoLocationRepository _geoLocationRepository;

  StreamSubscription? _geolocationSubscription;

  GeolocationBloc({required GeoLocationRepository geoLocationRepository})
      : _geoLocationRepository = geoLocationRepository,
        super(GeolocationLoading());

  @override
  Stream<GeolocationState> mapEventToState(GeolocationEvent event) async* {
    if (event is LoadedGeolocation) {
      yield* _mapLoadGeoLocationToState();
    } else if (event is UpdatedGeolocation) {
      yield* _mapUpdatedGeoLocationToState(event);
    }
  }

  Stream<GeolocationState> _mapLoadGeoLocationToState() async* {
    _geolocationSubscription?.cancel();

    final Position position = await _geoLocationRepository.getCurrentLocation();

    print("position.latitude");
    print(position.latitude);
    print(position.longitude);
    add(UpdatedGeolocation(position: position));
  }

  Stream<GeolocationState> _mapUpdatedGeoLocationToState(
      UpdatedGeolocation event) async* {
    yield GeolocationLoaded(position: event.position);
  }

  @override
  Future<void> close() {
    _geolocationSubscription?.cancel();
    return super.close();
  }
}
