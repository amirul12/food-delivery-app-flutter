part of 'geolocation_bloc.dart';

abstract class GeolocationEvent extends Equatable {
  const GeolocationEvent();

  @override
  List<Object?> get props => [];
}

class LoadedGeolocation extends GeolocationEvent {}

class UpdatedGeolocation extends GeolocationEvent {
  final Position position;

  UpdatedGeolocation({required this.position});

    @override
  List<Object?> get props => [position];
}
