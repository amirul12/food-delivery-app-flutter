import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/model/category_filter_model.dart';
import 'package:flutter_food_delivery_app/model/models.dart';
import 'package:flutter_food_delivery_app/model/price_filter_model.dart';

part 'filters_event.dart';
part 'filters_state.dart';

class FiltersBloc extends Bloc<FiltersEvent, FiltersState> {
  FiltersBloc() : super(FiltersLoading());

  @override
  Stream<FiltersState> mapEventToState(FiltersEvent event) async* {
    if (event is FilterLoad) {
      yield* _mapFilterLoadToState();
    }
    if (event is CategoryFilterUpdated) {
      yield* _mapCategoryFilterUpdateToState(event, state);
    }
    if (event is PriceFilterUpdated) {
      yield* _maoPriceFilterUpdateToState(event, state);
    }
  }

  Stream<FiltersState> _mapFilterLoadToState() async* {
    yield FiltersLoaded(
      filter: Filter(
        categoryFilters: CategoryFilter.filters,
        priceFilters: PriceFilter.filters,
      ),
    );
  }

  Stream<FiltersState> _mapCategoryFilterUpdateToState(
      CategoryFilterUpdated event, FiltersState state) async* {
    if (state is FiltersLoaded) {
      final List<CategoryFilter> updateCategoryFilters =
          state.filter.categoryFilters.map((category) {
        return category.id == event.categoryFilter.id
            ? event.categoryFilter
            : category;
      }).toList();

      yield FiltersLoaded(
          filter: Filter(
              categoryFilters: updateCategoryFilters,
              priceFilters: state.filter.priceFilters));
    }
  }

  Stream<FiltersState> _maoPriceFilterUpdateToState(
      PriceFilterUpdated event, FiltersState state) async* {
    if (state is FiltersLoaded) {
      final List<PriceFilter> updatePriceFilters =
          state.filter.priceFilters.map((priceFilter) {
        return priceFilter.id == event.priceFilter.id
            ? event.priceFilter
            : priceFilter;
      }).toList();

      print(updatePriceFilters);

      yield FiltersLoaded(
          filter: Filter(
              categoryFilters: state.filter.categoryFilters,
              priceFilters: updatePriceFilters));
    }
  }
}
