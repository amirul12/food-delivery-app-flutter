import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/model/place_autocomplete_model.dart';
import 'package:flutter_food_delivery_app/repository/place/place_repository.dart';

part 'autocomplete_event.dart';
part 'autocomplete_state.dart';

class AutocompleteBloc extends Bloc<AutocompleteEvent, AutocompleteState> {
  final PlaceRepository _placeRepository;

  StreamSubscription? _placeSubscription;

  AutocompleteBloc({required PlaceRepository placeRepository})
      : _placeRepository = placeRepository,
        super(AutocompleteLoading());

  @override
  Stream<AutocompleteState> mapEventToState(AutocompleteEvent event) async* {
    if (event is LoadedAutocomplete) {
      yield* _mapLoadAutocompleteToState(event);
    }
  }

  Stream<AutocompleteState> _mapLoadAutocompleteToState(
      LoadedAutocomplete event) async* {
    _placeSubscription?.cancel();
    final List<PlaceAutocomplete> autoComplete =
        await _placeRepository.getAutocomplete(event.searchInput);

    yield AutocompleteLoaded(autoComplete: autoComplete);
  }

  @override
  Future<void> close() {
    _placeSubscription?.cancel();
    return super.close();
  }
}
