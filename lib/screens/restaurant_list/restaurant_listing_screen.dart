import 'package:flutter/material.dart';
import 'package:flutter_food_delivery_app/model/models.dart';
import 'package:flutter_food_delivery_app/widget/widgets.dart';

class RestaurantListingScreen extends StatelessWidget {
  static const String routeName = '/restaurant-listing-screen';

  const RestaurantListingScreen({Key? key, required this.restaurants})
      : super(key: key);

  static Route route({required List<Restaurant> restaurants}) {
    return MaterialPageRoute(
        builder: (_) => RestaurantListingScreen(
              restaurants: restaurants,
            ),
        settings: const RouteSettings(name: routeName));
  }

  final List<Restaurant> restaurants;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Restaurants'),
      ),
      body: ListView.builder(
        itemCount: restaurants.length,
        itemBuilder: (context, index) {
          return RestaurantCard(restaurant: restaurants[index]);
        },
      ),
    );
  }
}
