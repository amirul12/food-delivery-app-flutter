import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/filters/filters_bloc.dart';
import 'package:flutter_food_delivery_app/model/category_model.dart';
import 'package:flutter_food_delivery_app/model/models.dart';
import 'package:flutter_food_delivery_app/model/price_model.dart';
import 'package:flutter_food_delivery_app/screens/restaurant_list/restaurant_listing_screen.dart';
import 'package:flutter_food_delivery_app/widget/widgets.dart';

class FilterScreen extends StatelessWidget {
  static const String routeName = '/filter';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => FilterScreen(),
        settings: const RouteSettings(name: routeName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: const Text('Filter'),
        ),
        bottomNavigationBar: BottomAppBar(
          child: BlocBuilder<FiltersBloc, FiltersState>(
            builder: (context, state) {
              if (state is FiltersLoading) {
                return CircularProgressIndicator();
              }
              if (state is FiltersLoaded) {
                return Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(),
                              primary: Theme.of(context).colorScheme.secondary,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 50)),
                          onPressed: () {
                            var categories = state.filter.categoryFilters
                                .where((filter) => filter.value)
                                .map((filter) => filter.category.name)
                                .toList();
                            print(categories);

                            var prices = state.filter.priceFilters
                                .where((price) => price.value)
                                .map((price) => price.price.price)
                                .toList();
                            print(prices);

                            List<Restaurant> restaurant = Restaurant.restaurants
                                .where(
                                  (restaurant) => categories.any((category) =>
                                      restaurant.tags.contains(category)),
                                )
                                .where(
                                  (restaurant) => prices.any(
                                    (price) => restaurant.priceCategory
                                        .contains(price),
                                  ),
                                )
                                .toList();

                            Navigator.pushNamed(
                                context, RestaurantListingScreen.routeName,
                                arguments: restaurant);
                          },
                          child: Text("Apply"))
                    ],
                  ),
                );
              } else {
                return Text("Something went wrong");
              }
            },
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Price",
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(color: Theme.of(context).colorScheme.secondary),
              ),
              CustomPriceFilter(),
              Text(
                "Category",
                style: Theme.of(context)
                    .textTheme
                    .headline3!
                    .copyWith(color: Theme.of(context).colorScheme.secondary),
              ),
              CustomCategoryFilter(),
            ],
          ),
        ));
  }
}
