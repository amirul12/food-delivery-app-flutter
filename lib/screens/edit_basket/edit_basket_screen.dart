import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/basket/basket_bloc.dart';

class EditBasketScreen extends StatelessWidget {
  static const String routeName = '/edit-basket';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => EditBasketScreen(),
        settings: const RouteSettings(name: routeName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: const Text('Edit Basket'),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.edit))],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(),
                      primary: Theme.of(context).colorScheme.secondary,
                      padding: const EdgeInsets.symmetric(horizontal: 50)),
                  onPressed: () {
                    // Navigator.pushNamed(context, BasketScreen.routeName);
                  },
                  child: Text("Done"))
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Items",
                style: Theme.of(context).textTheme.headline4!.copyWith(
                      color: Theme.of(context).colorScheme.secondary,
                    ),
              ),
              BlocBuilder<BasketBloc, BasketState>(
                builder: (context, state) {
                  if (state is BasketLoading) {
                    return CircularProgressIndicator();
                  } else if (state is BasketLoaded) {
                    return state.basket.items.length == 0
                        ? Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            width: double.infinity,
                            margin: const EdgeInsets.only(top: 5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 30, vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "No Items in the Basket.",
                                  textAlign: TextAlign.left,
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ],
                            ),
                          )
                        : ListView.builder(
                            itemCount: state.basket
                                .itemQuantity(state.basket.items)
                                .keys
                                .length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: Colors.white,
                                ),
                                width: double.infinity,
                                margin: const EdgeInsets.only(
                                  top: 5,
                                ),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                                child: Row(
                                  children: [
                                    Text(
                                      '${state.basket.itemQuantity(state.basket.items).entries.elementAt(index).value}x',
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .secondary),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Text(
                                        '${state.basket.itemQuantity(state.basket.items).keys.elementAt(index).name}x',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4,
                                      ),
                                    ),
                                    IconButton(
                                        visualDensity: VisualDensity.compact,
                                        onPressed: () {
                                          context.read<BasketBloc>()
                                            .add(RemoveAllItem(state.basket.itemQuantity(state.basket.items).keys.elementAt(index)));
                                        },
                                        icon: Icon(Icons.delete)),
                                    IconButton(
                                        visualDensity: VisualDensity.compact,
                                        onPressed: () {
                                          context.read<BasketBloc>()
                                            .add(AddItem(state.basket.itemQuantity(state.basket.items).keys.elementAt(index)));
                                        },
                                        icon: Icon(Icons.add_circle_outline)),
                                    IconButton(
                                        visualDensity: VisualDensity.compact,
                                        onPressed: () {
                                              context.read<BasketBloc>()
                                            .add(RemoveItem(state.basket.itemQuantity(state.basket.items).keys.elementAt(index)));
                             
                                        },
                                        icon:
                                            Icon(Icons.remove_circle_outline)),
                                  ],
                                ),
                              );
                            });
                  } else {
                    return Text("something went wrong");
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
