import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/autocomplete/autocomplete_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/basket/basket_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/filters/filters_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/geolocation/geolocation_bloc.dart';
import 'package:flutter_food_delivery_app/blocs/place/place_bloc.dart';
import 'package:flutter_food_delivery_app/config/app_router.dart';
import 'package:flutter_food_delivery_app/config/theme.dart';
import 'package:flutter_food_delivery_app/repository/geolocation/geolocation_repository.dart';
import 'package:flutter_food_delivery_app/repository/place/place_repository.dart';
import 'package:flutter_food_delivery_app/screens/home/home_screen.dart';
import 'package:flutter_food_delivery_app/screens/location/location_screen.dart';
import 'package:flutter_food_delivery_app/screens/restaurant_details/restaurant_details_screen.dart';
//amirul.m...bd@gmail.com for firebase, map

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<GeoLocationRepository>(
            create: (_) => GeoLocationRepository()),
        RepositoryProvider<PlaceRepository>(create: (_) => PlaceRepository()),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => GeolocationBloc(
                  geoLocationRepository: context.read<GeoLocationRepository>())
                ..add(LoadedGeolocation())),
          BlocProvider(
            create: (context) => AutocompleteBloc(
                placeRepository: context.read<PlaceRepository>())
              ..add(LoadedAutocomplete()),
          ),
          BlocProvider(
              create: (context) =>
                  PlaceBloc(placeRepository: context.read<PlaceRepository>())),
          BlocProvider(create: (context) => FiltersBloc()..add(FilterLoad())),
          BlocProvider(create: (context) => BasketBloc()..add(StartBasket())),
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: theme(),
          onGenerateRoute: AppRouter.onGenerateRoute,
          initialRoute: HomeScreen.routeName,
        ),
      ),
    );
  }
}
