import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/model/price_model.dart';

class PriceFilter extends Equatable {
  final int id;
  final Price price;
  final bool value;

  const PriceFilter(
      {required this.id, required this.price, required this.value});

  PriceFilter copyWith({
    int? id,
    Price? price,
    bool? value,
  }) {
    return PriceFilter(
        id: id ?? this.id,
        price: price ?? this.price,
        value: value ?? this.value);
  }

  @override
  List<Object?> get props => [id, price, value];

  static List<PriceFilter> filters = Price.prices
      .map((category) =>
          PriceFilter(id: category.id, price: category, value: false))
      .toList();
}
