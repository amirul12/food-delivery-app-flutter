import 'package:equatable/equatable.dart';

class Promo extends Equatable {
  final int id;
  final String title;
  final String description;
  final String imageUrl;

  Promo(
      {required this.id,
      required this.title,
      required this.description,
      required this.imageUrl});

  @override
  List<Object?> get props => [id, title, description, imageUrl];

  static List<Promo> promos = [
    Promo(
        id: 1,
        title: "FREE Delivery on Your First 3 Orders.",
        description:
            "Please an order of \$10 or more and the delivery fee is on us",
        imageUrl:
            'https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117'),
    Promo(
        id: 2,
        title: "20% OFF  First 3 Orders.",
        description:
            "Please an order of \$10 or more and the delivery fee is on us",
        imageUrl:
            'https://media.istockphoto.com/photos/table-top-view-of-spicy-food-picture-id1316145932?b=1&k=20&m=1316145932&s=170667a&w=0&h=feyrNSTglzksHoEDSsnrG47UoY_XX4PtayUPpSMunQI='),
    Promo(
        id: 3,
        title: "GET 2 BUY 1 on Your First 3 Orders.",
        description:
            "Please an order of \$10 or more and the delivery fee is on us",
        imageUrl:
            'https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117'),
    Promo(
        id: 4,
        title: "CASH BACK OFFER on Your First 3 Orders.",
        description:
            "Please an order of \$10 or more and the delivery fee is on us",
        imageUrl:
            'https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117'),
  ];
}
