import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/model/restaurant_model.dart';

class MenuItem extends Equatable {
  final int id;
  final int restaurantId;
  final String name;
  final String description;
  final String category;
  final double price;

  MenuItem(
      {required this.id,
      required this.restaurantId,
      required this.name,
      required this.category,
      required this.description,
      required this.price});

  @override
  List<Object?> get props =>
      [id, restaurantId, name, description, category, price];

  static List<MenuItem> menuItems = [
    MenuItem(
        id: 1,
        restaurantId: 1,
        name: "Pizza",
        category: "Pizza",
        description: "Pizza with Tomato",
        price: 5.99),
    MenuItem(
        id: 4,
        restaurantId: 2,
        name: "Pizza",
        category: "Pizza",
        description: "Pizza with Tomato",
        price: 5.99),
    MenuItem(
        id: 2,
        restaurantId: 1,
        name: "Hot cake",
        category: 'Drink',
        description: "Cake with Tomato",
        price: 1.99),
    MenuItem(
        id: 3,
        restaurantId: 3,
        name: "Water",
        category: 'Salad',
        description: "Cake with Tomato",
        price: 1.99),
  ];
}
