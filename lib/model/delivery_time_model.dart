import 'package:equatable/equatable.dart';

class DeliveryTime extends Equatable {
  final int id;
  final String value;
  final DateTime time;

  DeliveryTime({
    required this.id,
    required this.value,
    required this.time,
  });

  @override
  List<Object> get props => [id, value, time];

  static List<DeliveryTime> deliveryTimes = [
    DeliveryTime(
      id: 1,
      value: '08:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 20, 0),
    ),
    DeliveryTime(
      id: 2,
      value: '09:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 21, 0),
    ),
    DeliveryTime(
      id: 3,
      value: '07:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 19, 0),
    ),
    DeliveryTime(
      id: 4,
      value: '06:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 18, 0),
    ),
    DeliveryTime(
      id: 5,
      value: '05:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 17, 0),
    ),
    DeliveryTime(
      id: 6,
      value: '04:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 16, 0),
    ),
    DeliveryTime(
      id: 7,
      value: '03:00PM',
      time: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day, 15, 0),
    ),
  ];
}
