import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/model/category_filter_model.dart';
import 'package:flutter_food_delivery_app/model/price_filter_model.dart';

class Filter extends Equatable {
  final List<CategoryFilter> categoryFilters;
  final List<PriceFilter> priceFilters;

  Filter({required this.categoryFilters, required this.priceFilters});

  @override
  List<Object?> get props => [categoryFilters, priceFilters];

  Filter copyWith({
    List<CategoryFilter>? categoryFilters,
    List<PriceFilter>? priceFilters,
  }) {
    return Filter(
        categoryFilters: categoryFilters ?? this.categoryFilters,
        priceFilters: priceFilters ?? this.priceFilters);
  }
}
