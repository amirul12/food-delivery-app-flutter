import 'package:equatable/equatable.dart';
import 'package:flutter_food_delivery_app/model/menu_item_model.dart';

class Restaurant extends Equatable {
  final int id;
  final String imageUrl;
  final String name;
  final List<String> tags;
  final int deliveryTime;
  final String priceCategory;
  final List<MenuItem> menuItems;
  final double deliveryFee;
  final double distance;

  Restaurant(
      {required this.id,
      required this.imageUrl,
      required this.name,
      required this.tags,
      required this.menuItems,
      required this.deliveryTime,
      required this.priceCategory,
      required this.deliveryFee,
      required this.distance});

  // final List<MenuItem> menuItems;

  @override
  List<Object?> get props =>
      [id, imageUrl, name, tags, deliveryTime, deliveryFee, distance];

  static List<Restaurant> restaurants = [
    Restaurant(
        id: 1,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 1)
            .map((e) => e.category)
            .toSet()
            .toList(),
        deliveryTime: 30,
        priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 1)
            .toList(),
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 2,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 2)
            .map((e) => e.category)
            .toSet()
            .toList(),
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 2)
            .toList(),
                priceCategory: '\$',
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 3,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 3)
            .map((e) => e.category)
            .toSet()
            .toList(),
                priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 3)
            .toList(),
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 4,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 4)
            .map((e) => e.category)
            .toSet()
            .toList(),
                priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 4)
            .toList(),
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 5,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 5)
            .map((e) => e.category)
            .toSet()
            .toList(),
                priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 5)
            .toList(),
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 6,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 6)
            .map((e) => e.category)
            .toSet()
            .toList(),
                priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 6)
            .toList(),
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 7,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 7)
            .map((e) => e.category)
            .toSet()
            .toList(),
        deliveryTime: 30,
            priceCategory: '\$',
        deliveryFee: 2.90,
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 7)
            .toList(),
        distance: 0.1),
    Restaurant(
        id: 8,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 8)
            .map((e) => e.category)
            .toSet()
            .toList(),
                priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 8)
            .toList(),
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
    Restaurant(
        id: 9,
        imageUrl:
            "https://media.istockphoto.com/photos/restaurant-plates-picture-id104704117?s=612x612",
        name: "Golden Ice Gelato Artigianale",
        tags: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 9)
            .map((e) => e.category)
            .toSet()
            .toList(),
                priceCategory: '\$',
        menuItems: MenuItem.menuItems
            .where((menuItem) => menuItem.restaurantId == 9)
            .toList(),
        deliveryTime: 30,
        deliveryFee: 2.90,
        distance: 0.1),
  ];
}
